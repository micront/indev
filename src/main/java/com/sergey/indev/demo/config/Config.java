package com.sergey.indev.demo.config;


import indev.test.job.calc.ICalc;
import indev.test.job.calc.ThreadCalc;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.lang.reflect.Field;

@Configuration
public class Config {

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    ICalc iCalc() {
        try {
            Field field = ThreadCalc.class.getDeclaredField("_instance");
            field.setAccessible(true);
            field.set(ThreadCalc.class, null);
            field.setAccessible(false);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return ThreadCalc.Instance();
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    Object getObject() {
        return new Object();
    }
}
