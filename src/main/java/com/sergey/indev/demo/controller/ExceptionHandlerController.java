package com.sergey.indev.demo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.naming.OperationNotSupportedException;

@ControllerAdvice
public class ExceptionHandlerController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionHandlerController.class);

    /**
     * Handles exceptions.
     *
     * @param e - exception thrown
     * @return error code
     */
    @ExceptionHandler(OperationNotSupportedException.class)
    public ResponseEntity<String> handleValidationException(Exception e) {
        LOGGER.error("Validation exception, {}", e.getMessage(), e);

        return new ResponseEntity<>(e.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY);
    }
}
