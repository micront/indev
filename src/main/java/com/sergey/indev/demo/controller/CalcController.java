package com.sergey.indev.demo.controller;


import indev.test.job.calc.ICalc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalcController {

    private ICalc calc;

    @Lazy
    @Autowired
    CalcController(ICalc calc) {
        this.calc = calc;
    }

    @RequestMapping("/add")
    public String add(@RequestParam float a, @RequestParam float b) throws Exception {
        double result = calc.Add(a, b);
        return String.valueOf(result);
    }


    @RequestMapping("/subtraction")
    public String subtraction(@RequestParam float a, @RequestParam float b) throws Exception {
        double result = calc.Add(a, -b);
        return String.valueOf(result);
    }

    @RequestMapping("/multiply")
    public String multiply(@RequestParam float a, @RequestParam float b) throws Exception {
        double result = calc.Multiply(a, b);
        return String.valueOf(result);
    }

    @RequestMapping("/divide")
    public String divide(@RequestParam float a, @RequestParam float b) throws Exception {
        double result = calc.Divide(a, b);
        return String.valueOf(result);
    }
}
